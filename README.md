# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/creativehandles/ch-pages.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-pages)
[![Build Status](https://img.shields.io/travis/creativehandles/ch-pages/master.svg?style=flat-square)](https://travis-ci.org/creativehandles/ch-pages)
[![Total Downloads](https://img.shields.io/packagist/dt/creativehandles/ch-pages.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-pages)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require creativehandles/ch-pages
```

## Usage

This package depends on `creativehandles/ch-labels` and `creativehandles/ch-blog` packages.

- https://packagist.org/packages/creativehandles/ch-labels
- https://packagist.org/packages/creativehandles/ch-blog

If you already instaled `creativehandles/ch-labels` and `creativehandles/ch-blog`  packages in CORE CMS, 
proceed from step 3 below.

To activate Blog plugin with CORE CMS, you need to follow these steps.

1. Run `php artisan creativehandles:build-label-plugin`

2. Run `php artisan creativehandles:build-gallery-plugin`

3. Run `php artisan creativehandles:build-blog-plugin`

4. Run `php artisan creativehandles:build-pages-plugin`

5. Finally run `php artisan cms:populate-permissions` command to populate permission tables.

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email deemantha@creativehandles.com instead of using the issue tracker.

## Credits

- [Deemantha Kasun](https://github.com/creativehandles)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
