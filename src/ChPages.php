<?php

namespace Creativehandles\ChPages;

use Illuminate\Support\Facades\URL;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ChPages
{
    /**
     * Build FE base URL for page preview
     *
     * @return string
     */
    public function buildFeBaseUrl()
    {
        if (! empty(config('ch-pages.fe_base_url')) && ! empty(config('ch-pages.fe_pages_url'))) {
            return config('ch-pages.fe_base_url');
        } else {
            return URL::to('/');
        }
    }

    /**
     * Build page preview URL
     *
     * @param string $locale
     * @param int $pageId
     * @param string $pageSlug
     * @return string
     */
    public function buildPreviewUrl(string $locale = '', int $pageId = 1, string $pageSlug = '')
    {
        if (! empty(config('ch-pages.fe_base_url')) && ! empty(config('ch-pages.fe_pages_url'))) {
            return config('ch-pages.fe_base_url') . '/' . str_replace(['lang/', 'id', 'slug'], [$locale, $pageId, $pageSlug], config('ch-pages.fe_pages_url'));
        } else {
            return LaravelLocalization::getNonLocalizedURL(route('pages-preview-page', ['id' => $pageId, 'slug' => $pageSlug]));
        }
    }
}
