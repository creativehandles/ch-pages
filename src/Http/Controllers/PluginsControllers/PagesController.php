<?php

namespace Creativehandles\ChPages\Http\Controllers\PluginsControllers;

use App\Traits\MultilanguageTrait;
use Creativehandles\ChGallery\Plugins\Gallery\Gallery;
use Creativehandles\ChPages\Plugins\Pages\Models\PagesModel;
use Creativehandles\ChPages\Plugins\Pages\Pages;
use App\Traits\UploadTrait;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;
use ReflectionClass;
use Validator;
use Log;
use Auth;
use Carbon\Carbon;

class PagesController extends Controller
{
    use MultilanguageTrait, UploadTrait;

    /**
     * @var Pages
     */
    private $pages;

    protected $views = 'Admin.Pages.';
    protected $logprefix = "PagesController";
    protected $eventmodel;

    public function __construct(Pages $pages)
    {
        $this->setLocaleInRequestHeader();

        $this->pages = $pages;
        $reflector = new ReflectionClass(PagesModel::class);
        $this->eventmodel = $reflector->getShortName();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->pages();
    }

    /**
     *list all pages
     */
    public function pages()
    {
        $pages = $this->pages->getAllPages();
        return view($this->views . "pages-list")->with(['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $tags = $this->pages->getAllTags($this->eventmodel);
        $categories = $this->pages->getAllCategories();
        $availableId = $this->pages->getNextkey();
        return view($this->views . "pages")->with(compact('tags', 'categories','availableId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse|Response
     */
    public function store(Request $request)
    {
        //validate input data
        $validator = $this->validatePageData($request);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Page data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }
        //format input data
        list($formattedPage, $categories, $tags, $imageList) = $this->formatPageData($request);


        if ($request->has('page_image')) {
            // Get image file
            $image = $request->file('page_image');

            // Make a image name based on title and current timestamp
            $name = str_slug($request->input('title')) . '_' . time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set image path in database to filePath
            $formattedPage['page_image'] = $filePath;
        }

        try {
            if (! empty($formattedPage['id'])){
                $storedPage = $this->pages->updatePage($formattedPage);
            }

            $storedPage = $this->pages->storeNewPage($formattedPage);

            //link categories
            $storedPage->categories()->sync($categories);

            //link images
            if (count($imageList) > 0) {
                $this->processImages($imageList, $storedPage);
            } else {
                $storedPage->images()->where('model', $this->eventmodel)->delete();
            }

            //link tags
            if ($tags) {
                $taglist = [];

                foreach ($tags as $key=>$value){
                    $taglist[$value] = [
                        'model' => $this->eventmodel,
                        'ordinal' => $key + 1
                    ];
                }

                $storedPage->tags()->where('model', $this->eventmodel)->sync($taglist);
            }else{
                $storedPage->tags()->where('model', $this->eventmodel)->detach();
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => ['Error occurred'], 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => 'Page saved', 'data' => $storedPage], 200);
        } else {
            return redirect()->route('admin.pages')->with('success', 'Page created successfully!');
        }
    }

    protected function validatePageData(Request $request)
    {
        $rules = [
            'title' => 'required',
            'meta_description' => 'required',
            'body' => 'required',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function formatPageData(Request $request): array
    {
        $formattedPage = [
            'locale' => $request->input('form_locale'),
            'page_title' => $request->title,
            'page_meta_title' => ($request->meta_title) ? $request->meta_title : $request->title,
            'page_meta_description' => $request->meta_description,
            'page_visibility' => $request->visibility,
            'page_body' => $request->body,
            'page_author' => Auth::user()->id,
            'page_feature_media_type' => $request->feature_type,
            'page_video' => $request->page_video,
            'page_read_time' => $request->page_read_time,
            'page_date' => ($request->page_date) ? Carbon::parse($request->page_date)->toDateTimeString() : Carbon::now()->toDateTimeString(),
            'id' => $request->source_id ?? null
        ];

        $imageList = $request->get('multiImageList', []);

        $categories = $request->categories;

        $tags = $request->input('tags', []);

        foreach ($tags as $key => $tag) {
            if (is_string($tag)) {
                $data = [
                    'model'=> $this->eventmodel,
                    'locale' => $request->form_locale,
                    'label'=> $tag,
                    'author'=> \Auth::id(),
                    'color'=> sprintf('#%06X', mt_rand(0, 0xFFFFFF))
                ];

                $condition=[
                    'model'=> $this->eventmodel,
                    'locale' => $request->form_locale,
                    'label'=> $tag
                ];

                $storedTag = $this->pages->createNewTag($data,$condition);
                $tags[$key] = $storedTag->id;
            }
        }

        return array($formattedPage, $categories, $tags, $imageList);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($pageSlug)
    {
        $page = $this->pages->getPageBySlug($pageSlug);

        return $page;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $pageId
     * @return Response
     */
    public function edit(Request $request, $pageId)
    {
        $tags = $this->pages->getAllTags($this->eventmodel);
        $categories = $this->pages->getAllCategories();
        $page = $this->pages->getPageById($pageId);
        $page->page_image_url = asset($page->page_image);

        if ($request->ajax()) {
            if (isset($page)) {
                $page->setDefaultLocale($request->get('form_locale', app()->getLocale()));
                $pageData = $page->toArray();

                unset($pageData['images'], $pageData['tags'], $pageData['categories'], $pageData['author'], $pageData['translations']);

                return response()->json(['data' => $pageData], 200);
            } else {
                return response()->json(['msg' => [__('pages.404')]], 404);
            }
        }

        return view($this->views . "page-edit")->with(compact('tags', 'categories', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $pageId
     * @return Response
     */
    public function update(Request $request, $pageId)
    {
        //validate input data
        $validator = $this->validatePageData($request);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Page data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }


        //format form input data
        list($formattedPage, $categories, $tags,$imageList) = $this->formatPageData($request);


        if ($request->file('page_image')) {
            // Get image file
            $image = $request->file('page_image');

            // Make a image name based on title and current timestamp
            $name = str_slug($request->input('title')) . '_' . time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set image path in database to filePath
            $formattedPage['page_image'] = $filePath;
        } else {
            $formattedPage['page_image'] = $request->get('page_image_old');
        }

        //unset slug changes for the moment
//        unset($formattedPage['page_slug']);


        try {
            $formattedPage['id'] = $pageId;
            $updatePage = $this->pages->updatePage($formattedPage);

            //link categories
            $updatePage->categories()->sync($categories);

            //link images
            if (count($imageList) > 0) {
                $this->processImages($imageList, $updatePage);
            }else{
                $updatePage->images()->where('model', $this->eventmodel)->delete();
            }
            //link tags
            if ($tags) {
                $taglist=[];
                foreach ($tags as $key=>$value){
                    $taglist[$value]=[
                        'model'=>$this->eventmodel,
                        'ordinal'=>$key+1
                    ];
                }

                $updatePage->tags()->where('model',$this->eventmodel)->sync($taglist);
            }else{
                $updatePage->tags()->where('model',$this->eventmodel)->detach();
            }

        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured", 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', "Error Occured");
            }

        }
        if ($request->ajax()) {
            return response()->json(['msg' => "Page Saved"], 200);
        } else {
            return redirect()->route('admin.pages')->with('success', "Page Updated");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($pagdId)
    {
        $deletedPage = $this->pages->deletePageById($pagdId);

        if (!$deletedPage) {
            return response()->json(['msg' => "Error Occured"], 400);
        }

        return response()->json(['msg' => "Page Deleted"], 200);
    }


    public function searchPage(Request $request)
    {
        $param = $request->input('term');

        $page = $this->pages->getPageByName(array_get($param, 'term', ''));

        $page = $page->map(function ($page) {

            //$page->id = $page->id;
            $page->text = $page->page_title;

            return collect($page->toArray())
                ->only(['id', 'page_slug', 'text'])
                ->all();
        });
        return response()->json($page);
    }


    /**
     * Page category related methods
     */

    /**
     * Category related functions start
     */


    /**
     * new category view
     * @return Factory|View
     */
    public function createPageCategory()
    {
        $parentCat = request()->get('parentCat', null);
        $getChild = request()->get('getChild', null);

        if ($parentCat) {
            $categories = $this->pages->getCategoryById($parentCat)->children;
        } else {
            $categories = $this->pages->getAllCategories();
        }

        return view($this->views . "category")->with(compact('categories', 'parentCat'));
    }

    /**
     * update/create new category
     * @param Request $request
     * @return JsonResponse
     */
    public function storePageCategory(Request $request)
    {

        $validator = $this->validateCategory($request);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Page Category data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        $formattedCategory = $this->getFormattedCategory($request);

        $storedCategory = $this->pages->storeNewCategory($formattedCategory);

        if (!$storedCategory) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured"], 400);
            } else {
                return redirect()->back()->withInput()->with('error', 'Error Occured');
            }
        }

        $category = $storedCategory->toArray();
        $category['parent'] = ($storedCategory->parent) ? $storedCategory->parent->category_name : null;

        if ($request->ajax()) {
            return response()->json(['msg' => "Category Saved", 'data' => $category], 200);
        } else {
            return redirect()->route('admin.createPageCategory')->with('success', "Category Saved");
        }
    }

    public function editPageCategory(Request $request, $categoryId)
    {
        $category = $this->pages->getCategoryById($categoryId);

        if ($request->ajax()) {
            if (isset($category)) {
                $category->setDefaultLocale($request->get('form_locale', app()->getLocale()));
                return response()->json(['data' => $category], 200);
            } else {
                return response()->json(['msg' => [__('category.404')]], 404);
            }
        }

        $categories = $this->pages->getAllCategories();
        return view($this->views . 'category-edit')->with(compact('category', 'categories'));
    }

    public function updatePageCategory(Request $request, $categoryId)
    {
        $validator = $this->validateCategory($request);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Category data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        $formattedCategory = $this->getFormattedCategory($request);
        $formattedCategory['id'] = $categoryId;

        // unset slug
        // un-setting slug is removed as different languages should have language specific slugs
        // unset($formattedCategory['category_slug']);

        $updatedCategory = $this->pages->updateCategory($formattedCategory);

        if (!$updatedCategory) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured"], 400);
            } else {
                return redirect()->back()->withInput()->with('error', "Error Occured");
            }
        }
        if ($request->ajax()) {
            return response()->json(['msg' => "Category Updated"], 200);
        } else {
            return redirect()->route('admin.createPageCategory')->with('success', "Category Updated");
        }
    }

    public function deletePageCategory($categoryId)
    {
        $deletedCategory = $this->pages->deleteCategory($categoryId);

        if (!$deletedCategory) {
            return response()->json(['msg' => "Error Occured"], 400);
        }

        return response()->json(['msg' => "Category Deleted"], 200);
    }

    /**
     * get all categories by ajax call
     * @return mixed
     */
    public function getAjaxPageCategories()
    {
        $categories = $this->pages->getAllCategories();

        return $categories->pluck('category_name', 'id')->all();
    }

    /**
     * @param Request $request
     * @return JsonResponse|null
     */
    protected function validateCategory(Request $request)
    {
        $rules = [
            'category-name' => 'required',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getFormattedCategory(Request $request): array
    {
        return [
            'locale' => $request->input('form_locale'),
            'category_name' => $request->input('category-name'),
            'parent_id' => ($request->input('parent_category') != null) ? $request->input('parent_category') : 0,
            'category_description' => $request->input('category-description'),

            'seo_title' => $request->input('seo_title'),
            'seo_description' => $request->input('seo_description'),
        ];
    }

    /**
     * @param $imageList
     * @param $storedPage
     * @param $images
     */
    protected function processImages($imageList, $storedPage)
    {
        $images = [];

        foreach ($imageList as $image => $path) {
            $images[] = [
                'model_id' => $storedPage->id,
                'model' => $this->eventmodel,
                'image_path' => $path
            ];
        }

        $storedPage->images()->where('model', $this->eventmodel)->delete();
        $storedPage->images()->where('model', $this->eventmodel)->insert($images);
    }
}
