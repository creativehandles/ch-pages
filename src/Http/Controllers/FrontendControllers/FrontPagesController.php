<?php

namespace Creativehandles\ChPages\Http\Controllers\FrontendControllers;

use Creativehandles\ChPages\Plugins\Pages\Pages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use function view;

class FrontPagesController extends Controller
{


    /**
     * @var Pages
     */
    private $pages;

    public function __construct(Pages $pages)
    {

        $this->pages = $pages;
    }

    public function show($id,$slug)
    {
        $data = $this->pages->getPageById($id);

        //should return the view as follow
        //view('blog-view')->with(compact('$data'))
        return $data;
    }
}
