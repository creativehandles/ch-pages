<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 8/6/19
 * Time: 11:43 AM
 */

namespace Creativehandles\ChPages\Plugins\Pages;


use App\Repositories\Exceptions\EloquentRepositoryException;
use Creativehandles\ChLabels\Plugins\Labels\Repositories\LabelRepository;
use Creativehandles\ChPages\Plugins\Pages\Repositories\PageCategoriesRepository;
use Creativehandles\ChPages\Plugins\Pages\Repositories\PagesRepository;
use Creativehandles\ChPages\Plugins\Plugin;

class Pages extends Plugin
{

    const VISIBLE_TO_ALL = 1;
    const HIDDEN_TO_PUBLIC = 2;
    /**
     * @var PagesRepository
     */
    private $pagesRepository;
    /**
     * @var PageCategoriesRepository
     */
    private $categoriesRepository;

    /**
     * @var LabelRepository
     */
    private $labelRepository;

    public function __construct(
        PagesRepository $pagesRepository,
        PageCategoriesRepository $categoriesRepository,
        LabelRepository $labelRepository
    ) {
        parent::__construct();
        $this->pagesRepository = $pagesRepository;
        $this->categoriesRepository = $categoriesRepository;
        $this->labelRepository = $labelRepository;
    }

//    public function getAllPages()
//    {
//        return $this->pagesRepository->allWithPagination(16);
//    }

    public function storeNewCategory($data)
    {
        return $this->categoriesRepository->updateOrCreate($data, 'category_name');
    }


    public function getAllCategories()
    {
        return $this->categoriesRepository->all();
    }

    public function getAllParentCategories()
    {
        return $this->categoriesRepository->all();
    }

    public function getCategoryById($id)
    {
        return $this->categoriesRepository->find($id);
    }

    public function updateCategory($data)
    {
        return $this->categoriesRepository->updateOrCreate($data, 'id');
    }

    public function deleteCategory($id)
    {
        return $this->categoriesRepository->deleteById($id);
    }


    /**
     * Page Related functions start
     */

    /**
     * store new page in DB
     *
     * @param $data
     * @return mixed
     * @throws EloquentRepositoryException
     */
    public function storeNewPage($data)
    {
        return $this->pagesRepository->updateOrCreate($data,'id');
    }

    public function updatePage($data)
    {
        return $this->pagesRepository->updateOrCreate($data, 'id');
    }

    public function getNextkey()
    {
        return $this->pagesRepository->getNextkey();
    }

    public function getAllPages($limit=15)
    {
        return $this->pagesRepository->allWithPagination($limit);
    }

    public function getAllPagesWithoutPagination()
    {
        return $this->pagesRepository->all();
    }

    public function getPagesByVisibility($visibility=true)
    {
        return $this->pagesRepository->findBy('page_visibility',(int) $visibility);
    }

    public function getRelatedSlugs($title)
    {
        return $this->pagesRepository->findByLike('page_slug', $title)->pluck('page_slug');
    }

    public function getPageBySlug($slug, $with = ['tags', 'categories', 'author'])
    {
        return $this->pagesRepository->findBy('page_slug', $slug, $with);
    }

    public function getPageById($id)
    {
        return $this->pagesRepository->find($id, ['images', 'tags', 'categories', 'author']);
    }

    public function getPageByName($name)
    {
        return $this->pagesRepository->findByLike('page_title', '%' . $name . '%');
    }

    public function deletePageById($id)
    {
        return $this->pagesRepository->deleteById($id);
    }

    public function getRecentPagesExceptReadingOne(
        $field,
        $condition,
        $value,
        $limit,
        $with = ['tags', 'categories', 'author']
    ) {
        return $this->pagesRepository->getRecentPagesExceptReadingOne('updated_at', 'desc', $with, $limit, $field,
            $condition, $value);
    }


    public function getRecentPages($limit, $with = [])
    {
        return $this->pagesRepository->getRecentPages('created_at', $limit, $with);
    }
    /**
     * Tag related functions start
     */


    /**
     *
     * create new tag in DB
     * @param $tag
     * @return bool
     */
    public function createNewTag($data,$condition)
    {
        return $this->labelRepository->updateOrCreateByMultipleKeys($condition,$data);
    }

    public function getAllTags($model=null)
    {
        if($model){
            return $this->labelRepository->getBy('model',$model);
        }

        return $this->labelRepository->all();
    }

    public function getTag($id)
    {
        return $this->labelRepository->find($id);
    }


}
