<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 8/6/19
 * Time: 12:11 PM
 */

namespace Creativehandles\ChPages\Plugins\Pages\Repositories;

use App\Repositories\BaseEloquentRepository;
use App\Repositories\Exceptions\EloquentRepositoryException;
use Creativehandles\ChPages\Plugins\Pages\Models\PagesModel;
use Illuminate\Database\Eloquent\Model;

class PagesRepository extends BaseEloquentRepository
{
    /**
     * Get the model to be used for the repository;
     *
     * @return PagesModel
     */
    public function getModel()
    {
        return new PagesModel();
    }

    /**
     * Update or create a page category instance
     *
     * @param array $input
     * @param string $key
     * @param bool $toArray
     *
     * @return Model|array|bool
     */
    public function updateOrCreate($input, $key = 'id', $toArray = false)
    {
        if (!is_array($input) || empty($input)) {
            return false;
        }

        if (! empty($input[$key])) {
            if (in_array($key, $this->model->translatedAttributes, true)) {
                $page = get_class($this->model)::whereTranslation($key, $input[$key], app()->getLocale())->first();
                $page = $page ?: $this->getModel();
            } else {
                $page = $this->model->firstOrNew([$key => $input[$key]]);
            }
        } else {
            $page = $this->getModel();
        }

        $locale = $input['locale'];
        $pageTranslatable = $page->translateOrNew($locale);

        unset($input['id'], $input['locale']);

        $pageTranslatable->fill($input);
        $page->save();
        $page->setDefaultLocale($locale);

        return $page;
    }

    /**
     * Find a page using LIKE operator
     *
     * @param string $field
     * @param string $string
     * @return Model
     */
    public function findByLike($field, $string)
    {
        if (in_array($field, $this->model->translatedAttributes, true)) {
            return get_class($this->model)::whereTranslationLike($field, '%' . $string . '%', app()->getLocale())->get();
        } else {
            return parent::findByLike($field, $string);
        }
    }

    /**
     * Find page by field
     *
     * @param string $field
     * @param string $value
     * @param array $with
     * @return Model
     */
    public function findBy($field, $value, $with = [])
    {
        if (in_array($field, $this->model->translatedAttributes, true)) {
            return get_class($this->model)::whereTranslation($field, $value, app()->getLocale())->get();
        } else {
            return parent::findBy($field, $value);
        }
    }
}
