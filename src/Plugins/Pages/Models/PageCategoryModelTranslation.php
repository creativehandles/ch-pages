<?php

namespace Creativehandles\ChPages\Plugins\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class PageCategoryModelTranslation extends Model
{
    protected $table = 'page_category_translations';
    public $fillable = ['category_name', 'category_slug', 'category_description', 'seo_title', 'seo_url', 'seo_description'];
}
