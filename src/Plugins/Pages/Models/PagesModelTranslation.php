<?php

namespace Creativehandles\ChPages\Plugins\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class PagesModelTranslation extends Model
{
    protected $table = 'page_translations';
    public $fillable = ['page_title', 'page_slug', 'page_feature_media_type', 'page_image', 'page_video', 'page_meta_title', 'page_meta_description', 'page_visibility', 'page_body', 'page_author', 'page_read_time', 'page_date'];
}
