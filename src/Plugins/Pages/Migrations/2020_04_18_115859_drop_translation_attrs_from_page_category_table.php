<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTranslationAttrsFromPageCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // we drop the translation attributes in our main table:
        Schema::table('page_category', function ($table) {
            $table->dropColumn('category_name');
            $table->dropColumn('category_slug');
            $table->dropColumn('category_description');
            $table->dropColumn('seo_title');
            $table->dropColumn('seo_url');
            $table->dropColumn('seo_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // add the translatable attributes back in to main table
        Schema::table('page_category', function ($table) {
            $table->string('category_name', 191)->nullable()->default(null)->after('parent_id');
            $table->string('category_slug', 191)->nullable()->default(null)->after('category_name');
            $table->longText('category_description')->nullable()->default(null)->after('category_slug');
            $table->string('seo_title', 191)->nullable()->default(null)->after('category_description');
            $table->text('seo_url')->nullable()->default(null)->after('seo_title');
            $table->longText('seo_description')->nullable()->default(null)->after('seo_url');
        });
    }
}
