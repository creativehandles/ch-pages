<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTranslationAttrsFromPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // we drop the translation attributes in our main table:
        Schema::table('pages', function ($table) {
            $table->dropColumn('page_title');
            $table->dropColumn('page_slug');
            $table->dropColumn('page_feature_media_type');
            $table->dropColumn('page_image');
            $table->dropColumn('page_video');
            $table->dropColumn('page_meta_title');
            $table->dropColumn('page_meta_description');
            $table->dropColumn('page_language');
            $table->dropColumn('page_visibility');
            $table->dropColumn('page_body');
            $table->dropColumn('page_author');
            $table->dropColumn('page_read_time');
            $table->dropColumn('page_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // add the translatable attributes back in to main table
        Schema::table('pages', function ($table) {
            $table->string('page_title', 191)->after('id');
            $table->string('page_slug', 191)->after('page_title');
            $table->string('page_feature_media_type', 191)->nullable()->default('img')->after('page_slug');
            $table->text('page_image')->nullable()->default(null)->after('page_feature_media_type');
            $table->text('page_video')->nullable()->default(null)->after('page_image');
            $table->string('page_meta_title', 191)->nullable()->default(null)->after('page_video');
            $table->text('page_meta_description')->nullable()->default(null)->after('page_meta_title');
            $table->string('page_language', 191)->nullable()->default(config('app.locale'))->after('page_meta_description');
            $table->unsignedInteger('page_visibility')->nullable()->default(2)->after('page_language');
            $table->longText('page_body')->after('page_visibility');
            $table->unsignedInteger('page_author')->after('page_body');
            $table->string('page_read_time', 191)->nullable()->default(null)->after('page_author');
            $table->dateTime('page_date')->nullable()->default(null)->after('page_read_time');
        });
    }
}
