<?php

namespace Creativehandles\ChPages;

use Illuminate\Support\Facades\Facade;

/**
 * @method static ChPages buildFeBaseUrl()
 * @method static ChPages buildPreviewUrl(string $locale, int $pageId, string $pageSlug)
 *
 * @see \Creativehandles\ChPages\Skeleton\SkeletonClass
 */
class ChPagesFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-pages';
    }
}
