@extends('Admin.layout')

@section("styles")

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet"/>
    <style>
        .object_cover {
            object-fit: cover;
        }

        /*veritcal center*/
        .center-v {
            margin: 0;
            position: absolute;
            top: 50%;
            /*left: 50%;*/
            -ms-transform: translate(0%, -50%);
            transform: translate(0%, -50%);
        }
    </style>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs',['header'=>__('general.Page')])
                </div>
            </div>
        </div>

        <div class="content-header-right col-md-6 col-12 mb-2">
            <div class="mb-1 pull-right">
                <a href="{{route('admin.createPage')}}"
                   class="btn btn-secondary btn-block-sm"><i
                            class="ft-file-plus"></i> {{__('pages.Create Page')}}</a>

                <a href="{{route('admin.createPageCategory')}}"
                   class="btn btn-secondary btn-block-sm"><i
                            class="ft-layers"></i> {{__('pages.Manage Page Categories')}}</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        @if($pages->count() > 0)
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form action="" id="seachPageForm">
                                    <div class="form-body">

                                        <div class="form-group">
                                            {{--<input type="text" name="filter" class="form-control" placeholder="">--}}
                                            <lable for="seachPageSelect" class="display-inline-block mb-1">{{__('pages.Search For Pages')}}
                                            </lable>
                                            <select name="search" class="select2 form-control select2-hidden-accessible"
                                                    tabindex="-1" aria-hidden="true" placeholder="{{__('pages.Search For Pages')}}"
                                                    id="seachPageSelect">
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        @endif

        <div class="row">
            @if($pages->count() <= 0)
                <div class="col-md-12 text-center">
                    <p class="text-center">{{__('pages.No pages to display. Want to create a new one? Click the button below')}} </p>
                    <a href="{{route('admin.createPage')}}" class="btn btn-primary"> {{__('pages.Create an page')}}</a>
                </div>
            @else
                <div class="col-md-12">
                    <div class="row match-height">
                        @foreach($pages as $page)
                            <div class="col-xl-4 col-md-6 col-lg-4 col-sm-12">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="card-img-top img-fluid height-200 object_cover"
                                             src="{{$page->featured_media}}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h4 class="card-title">{{$page->page_title}}</h4>
                                            <p class="text-left">{{strtoupper($page->page_language)}}</p>
                                            <p class="text-left">
                                                {{Carbon\Carbon::parse($page->page_date?? $page->created_at)->toDateString()}}
                                            </p>

                                            <div class="row match-height">

                                                <div class="col align-middle">
                                                    @switch($page->page_visibility)
                                                        @case  (1)
                                                        <p class="badge badge-pill center-v badge-info ">{{__('pages.Visible to all')}}</p>
                                                        @break
                                                        @case (2)
                                                        <p class="badge badge-pill center-v badge-warning">{{__('pages.Hidden to public')}}</p>
                                                        @break
                                                    @endswitch
                                                </div>
                                                <div class="col">
                                                    <div class="btn-group pull-right" role="group"
                                                         aria-label="Basic example">
                                                        <a class="btn btn-md btn-outline-primary"
                                                           href="{{route('admin.editPage',['pageId'=>$page->id])}}"><i
                                                                    class="fa fa-edit"></i></a>
                                                        @if($page->locale === app()->getLocale())
                                                        <a class="btn btn-md btn-outline-danger delete-page"
                                                           data-id="{{$page->id}}"
                                                           href=""><i class="fa fa-trash"></i></a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 pagination-col">
                {{ $pages->links() }}
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{asset('vendors/js/forms/select/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var url = '{{route('admin.ajaxSearchPage')}}';

            $("#seachPageSelect").select2({
                minimumInputLength: 2,language:"{{ app()->getLocale() }}",
                allowClear: true,
                placeholder: "{{__('pages.Search for page')}}",
                ajax: {
                    url: url,
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    // cache: true
                }
            });

            var editPage = "{{ route('admin.editPage',['pageId'=>'sampleId']) }}";
            $('#seachPageSelect').on('select2:select', function (e) {
                var data = e.params.data;
                location.href = editPage.replace('sampleId',data.id);
            });

            $('#seachPageSelect').on('blur', function (e) {
                e.preventDefault();
                $('#seachPageSelect').select2("close")
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.delete-page').on('click', function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                var deleteRoute = "{{route('admin.deletePage',['pageId'=>'sampleId'])}}";

                swal({
                    title: "{{__('general.Warning!')}}",
                    text: "Are you sure you need to delete this page?",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "{{__('general.Cancel')}}",
                            value: null,
                            visible: !0,
                            className: "",
                            closeModal: !1
                        },
                        confirm: {
                            text: "{{__('general.Yes.Delete')}}",
                            value: !0,
                            visible: !0,
                            className: "",
                            closeModal: !1
                        }
                    }
                }).then(e => {
                    if (e) {
                        $.ajax({
                            dataType: 'json',
                            method: 'delete',
                            url: deleteRoute.replace('sampleId',id),
                        }).done(function (response) {
                            swal("{{__('general.Success!')}}", "{{__('general.Item deleted!!')}}", "success").then(() => {
                                location.reload();
                            });
                        }).fail(function (erroErrorr) {
                            swal("{{__('general.Error')}}", "{{__('general.Error Occured')}}", "error");
                        });
                    } else {
                        swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                    }
                });

            });


        });

    </script>
@endsection
