@extends('Admin.layout')

@section("styles")
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/summernote/summernote-lite.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/extensions/toastr.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("css/plugins/extensions/toastr.css") }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>

@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header'=>__('general.Page Category')])

    <div class="row">
        <div class="content-header-right col-md-12 col-12 mb-2">
            <div class="mb-1 pull-right">
                <button id="create-category-btn" class="btn btn-secondary btn-block-sm"><i
                            class="ft-file-plus"></i> {{__('category.Create Page Category')}}
                </button>
            </div>
        </div>
    </div>

    <div class="content-body">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('category.New page category')}}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse" id="new-category-form-card">
                        <div class="card-body">
                            @include('Admin.Pages.new-category-form')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('category.Categories')}}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th>{{__('category.Category')}}</th>
                                        @if($parentCat)
                                            <th>{{__('category.Parent Category')}}</th>
                                        @endif
                                        <th>{{__('category.Children Categories')}}</th>
                                        <th>{{__('category.URL')}}</th>
                                        <th>{{__('category.Action')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td>
                                                @if($category->children->count() > 0)
                                                    <a href="{{ route('admin.createPageCategory', ['parentCat' => $category->id]) }}" title="click to see sub categories"> {{$category->category_name}}</a>
                                                @else
                                                    {{$category->category_name}}
                                                @endif
                                            </td>
                                            @if($parentCat && $category->parent)
                                                <td>
                                                    <a href="{{ route('admin.createPageCategory', ['parentCat' => $category->parent->parent ? $category->parent->parent->id : null]) }}" title="click to go back">{{$category->parent->category_name}}</a>
                                                </td>
                                            @endif
                                            <td>
                                                {{$category->children->count()}}
                                            </td>

                                            <td>
                                                {{$category->seo_url}}
                                            </td>
                                            <td>
                                                @if($category->id != 1)
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                        <a href="{{route('admin.editPageCategory',['categoryId'=>$category->id])}}"
                                                           class="btn btn-outline-primary category-edit"><i
                                                                    class="ft-edit"></i></a>
                                                        @if($category->locale === app()->getLocale())
                                                        <a class="btn btn-outline-danger category-delete"
                                                           data-id="{{$category->id}}"><i class="ft-trash"></i></a>
                                                        @endif
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("js/scripts/summernote/summernote-lite.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#create-category-btn').on('click', function () {
                $('#new-category-form-card').toggle('slow');
            });

            $('#category-name').on('keyup', function () {
                var cat_name = $('#category-name').val();
                $('#seo_title').val(cat_name);
                $('#category_slug').val(cat_name.split(' ').join('-'));
            });

            $('#category-description').on('keyup', function () {
                $('#seo_description').val($('#category-description').val());
            });

            $('#parentCategory').select2({
                placeholder: window.SELECT_PARENT_CATEGORY,
                closeOnSelect: true,
                allowClear: true,
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            var categoryForm = $('#category-form');

            {{--categoryForm.on('submit', function (e) {--}}
            {{--e.preventDefault();--}}
            {{--$.ajax({--}}
            {{--data: categoryForm.serialize(),--}}
            {{--dataType: 'json',--}}
            {{--method: 'post',--}}
            {{--url: "{{ route("createNewCategory") }}",--}}
            {{--}).done(function (response) {--}}
            {{--toastr.success('New category added', 'Success',{--}}
            {{--timeOut: 2000,--}}
            {{--fadeOut: 1000,--}}
            {{--progressBar:true,--}}
            {{--onHidden: function () {--}}
            {{--window.location.reload();--}}
            {{--}--}}
            {{--});--}}
            {{--categoryForm[0].reset();--}}
            {{--}).fail(function (error) {--}}
            {{--toastr.error('Error Occured', 'Error');--}}
            {{--});--}}
            {{--});--}}

            $('.category-delete').on('click', function () {
                var categoryId = $(this).data('id');
                var deleteRoute = "{{route('admin.deletePageCategory',['categoryId'=>'sampleId'])}}";
                swal({
                    title: "{{__('general.Warning!')}}",
                    text: "Are you sure you need to delete this category?",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "{{__('general.Cancel')}}",
                            value: null,
                            visible: !0,
                            className: "",
                            closeModal: !1
                        },
                        confirm: {
                            text: "{{__('general.Yes.Delete')}}",
                            value: !0,
                            visible: !0,
                            className: "",
                            closeModal: !1
                        }
                    }
                }).then(e => {
                    if (e) {

                        $.ajax({
                            dataType: 'json',
                            method: 'delete',
                            url: deleteRoute.replace('sampleId',categoryId),
                        }).done(function (response) {
                            swal("{{__('general.Success!')}}", "{{__('general.Item deleted!!')}}", "success").then(() => {
                                location.reload();
                            });
                        }).fail(function (error) {
                            swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                        });
                    } else {
                        swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                    }
                })

            });

        });
    </script>
@endsection
