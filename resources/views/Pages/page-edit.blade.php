@extends('Admin.layout')

@section("styles")
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/extensions/toastr.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("css/plugins/extensions/toastr.css") }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>

@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header' => $page->page_title,'params' => $page])


    <div class="content-body">
        <div class="row">
            <div class="col-md-12 form-card">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('pages.Edit page')}}</h4>

                        <div class="heading-elements">
                            <ul class="list-inline mb-0 hide">
                                <li>
                                    <a class="btn btn-primary pull-right" href="{{ route('pages-preview-page', ['id' => $page->id, 'slug' => $page->page_slug]) }}" target="_blank">{{__('pages.Preview')}}</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="{{ route("admin.updatePage",['pageId' => $page->id]) }}" method="post" id="postForm" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-body">
                                            {{csrf_field()}}
                                            {{method_field('PUT')}}

                                            <div class="row">
                                                <div class="col-md-6">
                                                    {{--form_locale--}}
                                                    @include('Admin.partials.multi-language-select-locale', ['setRowCol' => false, 'model' => $page, 'selectId' => 'page-locale'])
                                                </div>
                                                <div class="col-md-6">
                                                    {{--page visibility--}}
                                                    <div class="form-group">
                                                        <label>{{__('pages.Page visibility')}}</label>
                                                        <select required class="custom-select block" name="visibility">
                                                            <option disabled>Please select a value</option>
                                                            <option {{(1 ==$page->page_visibility) ? 'selected' : ''}} value="1">
                                                                {{__('pages.Visible to all')}}
                                                            </option>
                                                            <option {{(2 ==$page->page_visibility) ? 'selected' : ''}} value="2">
                                                                {{__('pages.Hidden for public')}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            {{--title--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page Title')}}</label>
                                                <input required type="text" class="form-control"
                                                       placeholder="Page Title"
                                                       name="title" value="{{$page->page_title}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('pages.Page URL')}}
                                                    <a title="show preview" id="show-preview" class="show-url-preview"
                                                        href="{{ \Creativehandles\ChPages\ChPagesFacade::buildPreviewUrl('', $page->id, $page->page_slug ?? '') }}"
                                                        target="_blank">
                                                        <i class="fa fa-external-link-square"></i>
                                                    </a>
                                                </label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="span-url">{{  \Creativehandles\ChPages\ChPagesFacade::buildPreviewUrl('', $page->id, '') }}</span>
                                                    </div>
                                                    <input required type="text" class="form-control" id="page_slug"
                                                           placeholder="Page URL"
                                                           name="page_slug" value="{{ $page->page_slug }}">
                                                </div>
                                            </div>
                                            {{--meta-title--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Meta Title')}}</label>
                                                <input type="text" class="form-control"
                                                       placeholder="Meta Title"
                                                       name="meta_title" value="{{$page->page_meta_title}}">
                                            </div>


                                            {{--meta description--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Meta description')}}</label>
                                                <textarea required type="text" class="form-control"
                                                          placeholder="Meta description"
                                                          name="meta_description"
                                                          rows="3">{{$page->page_meta_description}}</textarea>
                                            </div>

                                            <div class="row hide">
                                                <div class="col-md-6">
                                                    <label>{{__('pages.Page date')}}</label>
                                                    <input type="datetime" name="page_date" class="form-control" value="{{Carbon\Carbon::parse($page->page_date)->format('Y-m-d\TH:i')}}" placeholder="Page Date">
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{__('pages.Read time')}}</label>
                                                        <input type="text" name="page_read_time" class="form-control" value="{{$page->page_read_time}}" placeholder="{{__('pages.Estimated page read time')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">

                                                </div>
                                            </div>

                                            {{--featured type--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page Featured Media Type')}}</label>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" {{ ($page->page_feature_media_type == 'img')? 'checked' : '' }} required name="feature_type" value="img" class="feature_type">
                                                        {{__('pages.Image')}}
                                                    </label>
                                                </fieldset>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" required {{ ($page->page_feature_media_type == 'vdo')? 'checked' : '' }} name="feature_type" value="vdo" class="feature_type">
                                                        {{__('pages.Video')}}
                                                    </label>
                                                </fieldset>
                                            </div>

                                            {{--featured image--}}
                                            <div class="form-group hide" id="feature-img-container">
                                                <label>{{__('pages.Page Featured Image')}}</label>
                                                <input id="page_image" type="file" class="form-control" name="page_image">
                                                <input  type="hidden" class="form-control" name="page_image_old" value="{{$page->page_image}}">
                                                <img id="page_image_preview" src="{{asset($page->page_image)}}" alt="featured" class="height-150 img-thumbnail">
                                            </div>

                                            {{--featured video--}}
                                            <div class="form-group hide" id="feature-vdo-container">
                                                <label>{{__('pages.Page Featured Video')}}</label>
                                                <input id="" type="text" class="form-control" placeholder="Place Youtube Video URL" name="page_video" value="{{$page->page_video}}">
                                            </div>

                                            <div class="form-group">
                                                <div class="form-group mb-2 attachment-repeater">
                                                    <label>{{__('pages.Attachments')}}</label>
                                                    {!! Gallery::RenderMultipleImageSelector($page->images->pluck('image_path')->all()) !!}
                                                </div>
                                            </div>

                                            <div class="form-group options">
                                                <label for="categorySelector">{{__('pages.Page category')}}</label>
                                                <select class="select2 form-control" id="categoryList" multiple
                                                        name="categories[]">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}" {{(in_array($category->id,$page->categories->pluck('id')->all())) ? 'selected' : ''}}> <span>{{($category->parent)? $category->parent->category_name.' - ':''}}</span> {{$category->category_name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" id="newCategory"><b>{{__('pages.New category?')}}</b></a>
                                                    </div>
                                                </div>
                                            </div>

                                            {{--body--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page body')}}</label>
                                                <textarea name="body" id="postBody" cols="30" rows="10"
                                                          placeholder="body"
                                                          class="form-control summernote-body">{{$page->page_body}}</textarea>
                                            </div>
                                            {{--link to categories--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page Tags')}}</label>
                                                <select class=" form-control" id="tagSelector"
                                                        multiple="multiple" aria-hidden="true"
                                                        name="tags[]">
                                                    <?php
                                                    $selected = $page->tags->pluck('id')->all();
                                                    $tagsCollection = $tags;
                                                    $substracted = array_diff($tags->pluck('id')->all(),$selected);
                                                    $mergedTags = array_merge($selected,$substracted);
                                                    $newtags = $tagsCollection->sortBy(function($model) use ($mergedTags){
                                                        return array_search($model->getKey(), $mergedTags);
                                                    });
                                                    ?>
                                                    @foreach($newtags as $tag)
                                                        <option {{(in_array($tag->id,$page->tags->pluck('id')->all())) ? 'selected' : ''}}  value="{{$tag->label}}">{{$tag->label}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-actions left">
                                            <button type="reset" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> {{__('pages.Clear')}}
                                            </button>
                                            <a href="{{ URL::previous() }}"><button type="button" href="" class="btn btn-warning mr-1">
                                                    <i class="ft-arrow-left"></i> {{__('pages.Go Back')}}
                                                </button></a>
                                            <button type="button" class="btn btn-primary" id="saveForm">
                                                <i class="fa fa-check-square-o"></i> {{__('pages.Save')}}
                                            </button>
                                            {{--<button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                                <i class="fa fa-check-square-o"></i> {{__('pages.Update and Go back')}}
                                            </button>--}}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card hide" id="category-card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('pages.Create new Category')}}</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @include('Admin.Pages.new-category-form')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function () {
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            var changeSelector = '.show-url-preview';
            //app url
            var feBaseUrl = "{{ \Creativehandles\ChPages\ChPagesFacade::buildFeBaseUrl() }}";
            var urlPreview = $(changeSelector).attr('href');

            //set url on page load
            var currLocale = $('#page-locale').val();
            var replacedUrl = urlPreview.replace(feBaseUrl, feBaseUrl + '/' + currLocale);
            $(changeSelector).attr('href',replacedUrl);

            var spanCurUrl = $('#span-url').html();
            $('#span-url').html(spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currLocale));

            //set url when change page locale
            $('#page-locale').on('change',function(){
                var currentLang = $(this).val();
                urlPreview = $(changeSelector).attr('href');
                var res = urlPreview.replace(feBaseUrl, feBaseUrl + '/' + currentLang);
                //set href
                $(changeSelector).attr('href', spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currentLang) + $('#page_slug').val());

                $('#span-url').html(spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currentLang));
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        // console.log(e.target.result);
                        // $("#page_image").append('<img class="height-150 img-thumbnail" src="'+e.target.result+'">')
                        $('#page_image_preview').attr('src', e.target.result);
                        $('#page_image_preview').show();
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#page_image").change(function() {
                readURL(this);
            });

            var checkedFeature = $("input[name='feature_type']:checked").val();

            switch (checkedFeature) {
                case 'img':
                    $('#feature-vdo-container').hide();
                    $('#feature-img-container').show();
                    break;

                case 'vdo':
                    $('#feature-img-container').hide();
                    $('#feature-vdo-container').show();
                    break;
            }

            $('.feature_type').on('change',function(){
                var checkedValue = $(this).val();

                if(checkedValue == 'img'){
                    $('#feature-vdo-container').hide();
                    $('#feature-img-container').show();
                }else{
                    $('#feature-img-container').hide();
                    $('#feature-vdo-container').show();
                }
            });


            $('#category-name').on('keyup',function(){
                var cat_name = $('#category-name').val();
                $('#seo_title').val(cat_name);
                $('#category_slug').val($.slugify(cat_name));
            });

            $('#category-description').on('keyup',function () {
                $('#seo_description').val($('#category-description').val());
            });

            $('#page_slug').on('keyup',function(){
                $('#page_slug').val($.slugify($(this).val()));
            });

            $('#newCategory').on('click', function (e) {
                e.preventDefault();
                $('.form-card').removeClass('col-md-12');
                $('.form-card').addClass('col-md-8');
                $('#category-card').toggle();
            });

            //checkbox require  validation
            function checkboxRequired() {
                var requiredCheckboxes = $('.options :checkbox[required]');

                if (requiredCheckboxes.is(':checked')) {
                    requiredCheckboxes.removeAttr('required');
                } else {
                    requiredCheckboxes.attr('required', 'required');
                }

                requiredCheckboxes.change(function () {
                    if (requiredCheckboxes.is(':checked')) {
                        requiredCheckboxes.removeAttr('required');
                    } else {
                        requiredCheckboxes.attr('required', 'required');
                    }
                });
            };

            checkboxRequired();

            //new category form submission and append added category to page form
            var categoryForm = $('#category-form');

            categoryForm.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    data: categoryForm.serialize(),
                    dataType: 'json',
                    method: 'post',
                    url: "{{ route("admin.createNewPageCategory") }}",
                }).done(function (response) {
                    toastr.success('New category added', 'Success');
                    var data = response.data;

                    $('#categoryList').last().append(
                        `<option selected value="`+data.id+`">` + ((data.parent)? data.parent + ' - ' : '' )+data.category_name+`</option>`

                    );

                    $('.options :checkbox[required]').removeAttr('required');
                    categoryForm[0].reset();

                }).fail(function (error) {
                    toastr.error('Error Occured', 'Error');
                });
            });

            //select 2
            $('#parentCategory').select2({
                placeholder: window.SELECT_PARENT_CATEGORY,
                closeOnSelect: true,
                allowClear: true,
            });

            //select 2 tag initiator
            $('#tagSelector').select2({
                placeholder: window.SELECT_TAGS,
                tags: true
            });

            $('#categoryList').select2({
                placeholder: window.SELECT_CATEGORY,
                closeOnSelect: true,
                allowClear: true,
            });

            //select2 initiator
            $('#categorySelector').select2({
                placeholder: window.SELECT_CATEGORY,
                minimumResultsForSearch: Infinity,
            });

            //new page form submission

            var saveFormBtn = $('#saveForm');
            var saveBtnOriginal = saveFormBtn.html();

            $('#saveForm').on('click', function (e) {
                e.preventDefault();

                saveFormBtn.html('<i class="fa fa-spinner"></i> Processing');
                //new article form submission
                var postForm = $('#postForm')[0];

                // Create an FormData object
                var data = new FormData(postForm);
                $.ajax({
                    data: data,
                    dataType: 'json',
                    method: 'post',
                    processData: false,  // Important!
                    contentType: false,
                    url: "{{ route("admin.updatePage",['pageId' => $page->id]) }}",
                }).done(function (response) {
                    saveFormBtn.html(saveBtnOriginal);
                    toastr.success('Page Updated', 'Success', {
                        timeOut: 5000,
                        fadeOut: 1000,
                        progressBar: true,
                    });

                }).fail(function (error) {
                    saveFormBtn.html(saveBtnOriginal);
                    var messages = error.responseJSON.msg;

                    for (message in messages) {
                        toastr.error(messages[message], 'Error',{ timeOut: 5000  });
                    }
                });
            });

            //form reset button functionality
            $("button[type='reset']").on("click", function (event) {
                event.preventDefault();
                postForm[0].reset();
                $('#postBody').summernote("reset");
                $('#tagSelector').val('').trigger('change');
            });

            $('select[name="form_locale"]').on('change', function (e) {
                let slctLocale = $(this);
                let id = '{{ $page->id }}';
                let form_locale = slctLocale.val();
                let url = '{{ route('admin.editPage', ['pageId' => $page->id]) }}';

                $.ajax({
                    data: {_token: CSRF_TOKEN, id: id, form_locale: form_locale},
                    method: 'GET',
                    url: url,
                }).done(function (object) {
                    try {
                        setFormFields(slctLocale.closest('form'), object.data);
                    } catch (e) {
                        console.log(e);
                    }
                }).fail(function (error) {
                    console.log(error);
                    let messages = error.responseJSON.msg;

                    for (let message in messages) {
                        toastr.error(messages[message], 'Error', {timeOut: 5000});
                    }
                });
            });

            function setFormFields(form, page) {
                if (page.page_visibility) {
                    form.find('select[name="visibility"]').val(page.page_visibility);
                }

                if (page.page_title) {
                    form.find('input[name="title"]').val(page.page_title);
                }

                if (page.page_slug) {
                    form.find('input[name="page_slug"]').val(page.page_slug);
                }

                if (page.page_meta_title) {
                    form.find('input[name="meta_title"]').val(page.page_meta_title);
                }

                if (page.page_meta_description) {
                    form.find('textarea[name="meta_description"]').val(page.page_meta_description);
                }

                if (page.page_date) {
                    form.find('input[name="page_date"]').val(page.page_date);
                }

                if (page.page_read_time) {
                    form.find('input[name="page_read_time"]').val(page.page_read_time);
                }

                if (page.page_feature_media_type) {
                    form.find('input[name="feature_type"][value="' + page.page_feature_media_type + '"]').prop('checked', true);

                    switch (page.page_feature_media_type) {
                        case 'img':
                            form.find('#feature-vdo-container').hide();
                            form.find('#feature-img-container').show();
                            break;

                        case 'vdo':
                            form.find('#feature-img-container').hide();
                            form.find('#feature-vdo-container').show();
                            break;
                    }
                }

                if (page.page_image) {
                    form.find('input[name="page_image_old"]').val(page.page_image);
                    form.find('#page_image_preview').attr('src', page.page_image_url);
                }

                if (page.page_video) {
                    form.find('input[name="page_video"]').val(page.page_video);
                }

                if (page.page_body) {
                    tinyMCE.get('postBody').setContent(page.page_body);
                }
            }
        });
    </script>
@endsection
