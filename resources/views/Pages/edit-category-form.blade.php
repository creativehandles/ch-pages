<form method="post" id="category-form" action="{{route("admin.updatePageCategory",['categoryId'=>$category->id])}}">

    {{ csrf_field()  }}
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                @include('Admin.partials.multi-language-select-locale', ['setRowCol' => false, 'model' => $category])
                <div class="form-group">
                    <label>{{__('category.Category name')}}</label>
                    <input required type="text" class="form-control"
                           placeholder="{{__('category.Category name')}}"
                           name="category-name" id="category-name"  value="{{$category->category_name}}">
                </div>

                <div class="form-group">
                    <label>{{__('category.SEO title')}}</label>
                    <input required type="text" class="form-control"
                           placeholder="{{__('category.SEO title')}}"
                           name="seo_title" id="seo_title" value="{{$category->seo_title}}">
                </div>

                <div class="form-group">
                    <label>{{__('category.Category URL')}}</label>
                    <div class="row">
                        <div class="col-2">
                            <p class="form-control border-0 text-right">category/</p>
                        </div>
                        <div class="col-10">
                            <input required type="text" class="form-control"
                                   placeholder="{{__('category.Category URL')}}"
                                   name="category_slug" id="category_slug" value="{{$category->category_slug}}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>{{__('category.Parent Category')}}</label>
                    <select class="select2 form-control" id="parentCategory" name="parent_category">
                        <option></option>
                        @foreach($categories as $categoryl)
                            @if($categoryl->id != 1 && $categoryl->id != $category->id)
                                <option value="{{$categoryl->id}}" {{$categoryl->id == $category->parent_id ? 'selected' : '' }}> {{$categoryl->category_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>{{__('category.Category description')}}</label>
                    <textarea type="text" class="form-control"
                              placeholder="{{__('category.Category description')}}"
                              name="category-description" id="category-description" rows="3">{{$category->category_description}}</textarea>
                </div>
                <div class="form-group">
                    <label>{{__('category.SEO description')}}</label>
                    <textarea type="text" class="form-control"
                              placeholder="{{__('category.SEO description')}}"
                              name="seo_description" id="seo_description" rows="3">{{$category->seo_description}}</textarea>
                </div>
            </div>
            <div class="form-actions left">
                {{--<button type="reset" class="btn btn-warning mr-1">--}}
                {{--<i class="ft-x"></i> Cancel--}}
                {{--</button>--}}
                <button type="submit" class="btn btn-primary ">
                    <i class="fa fa-check-square-o"></i> {{__('category.Update category')}}
                </button>
            </div>
        </div>
    </div>
</form>
