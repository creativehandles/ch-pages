@extends('Admin.layout')

@section("styles")
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/extensions/toastr.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("css/plugins/extensions/toastr.css") }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header'=>__('general.Page Category'),'params'=>$category])
    <div class="content-body">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__("category.Edit page category")}}</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                           @include('Admin.Pages.edit-category-form')
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#parentCategory').select2({
                placeholder: '{{__("category.Select a parent category")}}',
                closeOnSelect: true,
                allowClear: true,
            });

            var categoryForm = $('#category-form');

            $('#category-name').on('keyup',function(){
                var cat_name = $('#category-name').val();
                $('#seo_title').val(cat_name);
                $('#category_slug').val(cat_name.split(' ').join('-'));
            });

            $('#category-description').on('keyup',function () {
                $('#seo_description').val($('#category-description').val());
            });

            $('select[name="form_locale"]').on('change', function (e) {
                let slctLocale = $(this);
                let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                let id = '{{ $category->id }}';
                let form_locale = slctLocale.val();
                let url = '{{ route('admin.editPageCategory', ['categoryId' => $category->id]) }}';

                $.ajax({
                    data: {_token: CSRF_TOKEN, id: id, form_locale: form_locale},
                    method: 'GET',
                    url: url,
                }).done(function (object) {
                    try {
                        if (object.data.category_name) {
                            slctLocale.closest('form').find('input[name="category-name"]').val(object.data.category_name);
                        }

                        if (object.data.seo_title) {
                            slctLocale.closest('form').find('input[name="seo_title"]').val(object.data.seo_title);
                        }

                        if (object.data.category_slug) {
                            slctLocale.closest('form').find('input[name="category_slug"]').val(object.data.category_slug);
                        }

                        if (object.data.parent_id) {
                            slctLocale.closest('form').find('select[name="parent_category"]').val(object.data.parent_id);
                        }

                        if (object.data.category_description) {
                            slctLocale.closest('form').find('textarea[name="category-description"]').val(object.data.category_description);
                        }

                        if (object.data.seo_description) {
                            slctLocale.closest('form').find('textarea[name="seo_description"]').val(object.data.seo_description);
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }).fail(function (error) {
                    console.log(error);
                    let messages = error.responseJSON.msg;

                    for (let message in messages) {
                        toastr.error(messages[message], 'Error', {timeOut: 5000});
                    }
                });
            });

            {{--categoryForm.on('submit', function (e) {--}}
                {{--e.preventDefault();--}}
                {{--$.ajax({--}}
                    {{--data: categoryForm.serialize(),--}}
                    {{--dataType: 'json',--}}
                    {{--method: 'put',--}}
                    {{--url: "{{ route("updateCategory",['categoryId'=>$category->id]) }}",--}}
                {{--}).done(function(response){--}}
                    {{--toastr.success('Category updated', 'Success');--}}
                    {{--location.href="{{route('createCategory')}}";--}}
                {{--}).fail(function (error) {--}}
                    {{--toastr.error('Error Occured', 'Error');--}}
                {{--});--}}
            {{--});--}}
        });
    </script>
@endsection
