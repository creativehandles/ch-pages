<form action="{{route('admin.createNewPageCategory')}}" method="post" id="category-form">

    {{ csrf_field()  }}
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                @include('Admin.partials.multi-language-select-locale', ['setRowCol' => false])
                <div class="form-group">
                    <label>{{__('category.Category name')}}</label>
                    <input required type="text" class="form-control"
                           placeholder="{{__('category.Category name')}}"
                           name="category-name" id="category-name" value="{{old('category-name')}}">
                </div>


                <div class="form-group">
                    <label>{{__('category.SEO title')}}</label>
                    <input required type="text" class="form-control"
                           placeholder="{{__('category.SEO title')}}"
                           name="seo_title" id="seo_title" value="{{old('seo_title')}}">
                </div>

                <div class="form-group">
                    <label>{{__('category.Category URL')}}</label>
                            <input required type="text" class="form-control"
                                   placeholder="{{__('category.Category URL')}}"
                                   name="category_slug" id="category_slug" value="{{old('category_slug')}}">
                </div>

                <div class="form-group">
                    <label>{{__('category.Parent Category')}}</label>
                    <select  class="select2 form-control"  id="parentCategory" name="parent_category">
                        <option></option>
                        @foreach($categories as $category)
                            @if($category->id != 1)
                                <option value="{{$category->id}}" {{old('parent_category') == $category->id ? 'selected' : '' }}> {{$category->category_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>{{__('category.Category description')}}</label>
                    <textarea type="text" class="form-control"
                              placeholder="{{__('category.Category description')}}"
                              name="category-description" id="category-description" rows="3">{{old('category-description')}}</textarea>
                </div>
                <div class="form-group">
                    <label>{{__('category.SEO description')}}</label>
                    <textarea type="text" class="form-control"
                              placeholder="{{__('category.SEO description')}}"
                              name="seo_description" id="seo_description" rows="3">{{old('seo_description')}}</textarea>
                </div>
            </div>
            <div class="form-actions left">
                {{--<button type="reset" class="btn btn-warning mr-1">--}}
                {{--<i class="ft-x"></i> Cancel--}}
                {{--</button>--}}
                <button type="submit" class="btn btn-primary ">
                    <i class="fa fa-check-square-o"></i> {{__('category.Add new category')}}
                </button>
            </div>
        </div>
    </div>
</form>
