<?php

Route::group(['name' => 'pages', 'groupName' => 'pages'], function () {
    //Page plugin related routes
    Route::get('/pages', 'PluginsControllers\PagesController@index')->name('pages');

    Route::get('/createPage', 'PluginsControllers\PagesController@create')->name('createPage');
    Route::post('/createPage', 'PluginsControllers\PagesController@store')->name('createNewPage');
    Route::get('/pages-list', 'PluginsControllers\PagesController@pages')->name('listPages');
    Route::get('/pages/search', 'PluginsControllers\PagesController@searchPage')->name('ajaxSearchPage');

    Route::get('/pages/edit/{pageId}', 'PluginsControllers\PagesController@edit')->name('editPage');
    Route::get('/pages/{pageSlug}', 'PluginsControllers\PagesController@show')->name('showPage');
    Route::put('/pages/{pageId}', 'PluginsControllers\PagesController@update')->name('updatePage');
    Route::delete('/pages/delete/{pageId}', 'PluginsControllers\PagesController@destroy')->name('deletePage');
});

Route::group(['name' => 'pageCategory', 'groupName' => 'pageCategory'], function () {
    //page category
    Route::get('/createPageCategory',
        'PluginsControllers\PagesController@createPageCategory')->name('createPageCategory');
    Route::post('/createPageCategory',
        'PluginsControllers\PagesController@storePageCategory')->name('createNewPageCategory');
    Route::get('/page-category/edit/{categoryId}',
        'PluginsControllers\PagesController@editPageCategory')->name('editPageCategory');
    Route::post('/page-category/{categoryId}',
        'PluginsControllers\PagesController@updatePageCategory')->name('updatePageCategory');
    Route::delete('/page-category/{categoryId}',
        'PluginsControllers\PagesController@deletePageCategory')->name('deletePageCategory');
    Route::get('/get-ajax-categories',
        'PluginsControllers\PagesController@getAjaxPageCategories')->name('getAjaxPageCategories');
});

