<?php

// Dashboard > pages
Breadcrumbs::for('admin.pages', function ($trail) {
//    $trail->parent('admin.dashboard');
    $trail->push(__('general.Pages'), route('admin.pages'));
});

Breadcrumbs::for('admin.createPage', function ($trail) {
    $trail->parent('admin.pages');
    $trail->push(__('general.New Page'), route('admin.createPage'));
});

Breadcrumbs::for('admin.showPage', function ($trail, $page) {
    $trail->parent('admin.pages');
    $trail->push(__('general.Page'), route('admin.showPage', $page->page_title ?? ''));
});

Breadcrumbs::for('admin.editPage', function ($trail, $page) {
    $trail->parent('admin.pages');
    $trail->push(__('general.Edit page'), route('admin.editPage', $page->page_title ?? ''));
});

//pages > Manage Category
Breadcrumbs::for('admin.createPageCategory', function ($trail) {
    $trail->parent('admin.pages');
    $trail->push(__('general.Manage categories'), route('admin.createPageCategory'));
});

//pages > Edit Category
Breadcrumbs::for('admin.editPageCategory', function ($trail, $category) {
    $trail->parent('admin.pages');
    $trail->push(__('general.Edit Category'), route('admin.editPageCategory', $category->category_name ?? ''));
});
