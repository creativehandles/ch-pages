<?php

use Illuminate\Support\Facades\Route;

// PAGES plugin
Route::get('/p/{id}/{slug?}','FrontendControllers\FrontPagesController@show')->name('pages-preview-page');
