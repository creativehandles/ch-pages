<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'fe_base_url' => env('FE_BASE_URL', ''),
    'fe_pages_url' => env('FE_PAGES_URL', '')
];
